'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = combineSectionReducers;

var _index = require('is-valid-redux-reducer/lib/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function combineSectionReducers(reducers) {
  var reducerKeys = Object.keys(reducers);

  var error = void 0;
  try {
    for (var i = 0; i < reducerKeys.length; i++) {
      (0, _index2.default)(reducers[reducerKeys[i]], true);
    }
  } catch (e) {
    error = e;
  }

  return function (state, action, entireState) {
    if (error) {
      throw error;
    }

    var hasChanged = false;
    var nextState = typeof state === 'undefined' ? {} : Object.assign({}, state);
    function forEachReducers(state, action, entireState) {
      for (var _i = 0; _i < reducerKeys.length; _i++) {
        var key = reducerKeys[_i];
        var previousStateForKey = state(key);
        var nextStateForKey = reducers[key](previousStateForKey, action, entireState);
        nextState[key] = nextStateForKey;
        hasChanged = hasChanged || nextStateForKey !== previousStateForKey;
      }
    }
    if (typeof state === 'undefined') {
      // Initialize
      forEachReducers(function (key) {
        return undefined;
      }, action, undefined);
    } else if (typeof entireState === 'undefined') {
      // Be used as a normal reducer
      forEachReducers(function (key) {
        return state[key];
      }, action, state);
    } else {
      // Be used as a section reducer
      forEachReducers(function (key) {
        return state[key];
      }, action, entireState);
    }

    return hasChanged ? nextState : state;
  };
}